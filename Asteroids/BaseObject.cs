﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace Asteroids
{
    public delegate void Message();

    /// <summary>
    ///     Объявление всех обыектов, присутствующих на форме 
    /// </summary>
    abstract class BaseObject : ICollision
    {
        protected Point Pos;
        protected Point Dir;
        protected Size Size;

        protected BaseObject(Point pos, Point dir, Size size)
        {
            Pos = pos;
            Dir = dir;
            try
            {
                Size = size;
                if (Size.Height < 0 || Size.Width < 0)
                {
                    throw new GameObjectException("Ввод отрицательных размеров объекта");
                }
            } catch (GameObjectException)
            {
                Size.Width = Size.Width < 0 ? -Size.Width : Size.Width;
                Size.Height = Size.Height < 0 ? -Size.Height : Size.Height;
            }
        }

        public Rectangle Rect => new Rectangle(Pos, Size);

        public bool Collision(ICollision obj) => obj.Rect.IntersectsWith(this.Rect);

        public abstract void Draw();
        public abstract void Update();
    }

    class Star: BaseObject
    {
        public Star(Point pos, Point dir, Size size): base(pos, dir, size)
        {
            
        }

        public override void Draw()
        {
            WinForm.Buffer.Graphics.DrawLine(Pens.White, Pos.X, Pos.Y, Pos.X + Size.Width, Pos.Y + Size.Height);
            WinForm.Buffer.Graphics.DrawLine(Pens.White, Pos.X + Size.Width, Pos.Y, Pos.X, Pos.Y + Size.Height);
        }

        public override void Update()
        {
            Pos.X = Pos.X + Dir.X;
            if (Pos.X < 0) Pos.X = WinForm.Width;
        }
    }

    class Earth: BaseObject
    {
        Image earth;

        public Earth(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
            Assembly _assembly = Assembly.GetExecutingAssembly();
            Stream _imageStream = _assembly.GetManifestResourceStream("Asteroids.earth.png");
            earth = new Bitmap(_imageStream);
        }

        public override void Draw()
        {
            WinForm.Buffer.Graphics.DrawImage(earth, Pos.X, Pos.Y, Size.Width, Size.Height);
        }

        public override void Update()
        {
            Random random = new Random();

            Pos.X = Pos.X + Dir.X;
            Pos.Y = Pos.Y + Dir.Y;
            if (Pos.X < 0 - 2*Size.Height)
            {
                Pos.Y = random.Next(WinForm.Height - 2*Size.Height);
                Pos.X = WinForm.Width;
            }
        }
    
    }

    class InteractionObject : BaseObject
    {
        Image img;
        public int Power { get; protected set; }

        protected InteractionObject(Point pos, Point dir, Size size, int power, String imgPath) : base(pos, dir, size)
        {
            Power = power;

            Assembly _assembly = Assembly.GetExecutingAssembly();
            Stream _imageStream = _assembly.GetManifestResourceStream(imgPath);
            img = new Bitmap(_imageStream);
        }

        public override void Draw()
        {
            WinForm.Buffer.Graphics.DrawImage(img, Pos.X, Pos.Y, Size.Width, Size.Height);
        }

        Random random = new Random();
        public override void Update()
        {
            Pos.X = Pos.X + Dir.X;
            Pos.Y = Pos.Y + Dir.Y;
            if (Pos.X < 0)
            {
                Pos.Y = random.Next(WinForm.Height - 2 * Size.Height);
                Pos.X = WinForm.Width;
            }

            if (Pos.Y < 0 || Pos.Y > WinForm.Height - 2 * Size.Height)
                Dir.Y = -Dir.Y;
        }
    }

    class Asteroid : InteractionObject
    {
        public Asteroid(Point pos, Point dir, Size size, int power, string imgPath = "Asteroids.resourses.spaceship.png") : base(pos, dir, size, power, imgPath )
        {
            if (Power < 0) Power = -Power;
        }
    }

    class MedicineChest : InteractionObject
    {
        public MedicineChest(Point pos, Point dir, Size size, int power, string imgPath = "Asteroids.resourses.heal.png") : base(pos, dir, size, power, imgPath)
        {
            if (Power > 0) Power = -Power;
        }
    }

    class Bullet : BaseObject
    {
        public Bullet(Point pos, Point dir, Size size) : base(pos, dir, size)
        {

        }

        public override void Draw()
        {
            WinForm.Buffer.Graphics.DrawRectangle(Pens.Orange, Pos.X, Pos.Y, Size.Width, Size.Height);
        }

        public override void Update()
        {
            Pos.X += Dir.X;
        }

        public void ShootDown()
        {
            Pos.X = 0;
        }
    }

    class GameObjectException : Exception
    {
        public GameObjectException(string message) : base(message)
        {

        }
    }


    class Spaceship : BaseObject
    {
        public static event Message MessageDie;

        Image spaceship;

        public int Energy { get; private set; } = 100;

        public Spaceship(Point pos, Point dir, Size size) : base(pos, dir, size)
        {
            Assembly _assembly = Assembly.GetExecutingAssembly();
            Stream _imageStream = _assembly.GetManifestResourceStream("Asteroids.resourses.spaceship.png");
            spaceship = new Bitmap(_imageStream);
        }

        public override void Draw()
        {
            WinForm.Buffer.Graphics.DrawImage(spaceship, Pos.X, Pos.Y, Size.Width, Size.Height);
        }

        public override void Update()
        {
        }

        public void EnergyLow(int n)
        {
            Energy -= n;
        }

        public void Up()
        {
            if (Pos.Y > 0) Pos.Y -= Dir.Y;
        }

        public void Down()
        {
            if (Pos.Y < WinForm.Height) Pos.Y += Dir.Y;
        }

        public void Die()
        {
            MessageDie?.Invoke();
        }
    }
}
