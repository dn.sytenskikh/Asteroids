﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Asteroids
{
    /// <summary>
    ///     Хранимка параметров формы, инициализирует форму, создает буфер
    /// </summary>
    static class WinForm
    {

        private static BufferedGraphicsContext _context;
        public static BufferedGraphics Buffer;
        public static Form form;

        public static int Width { get; set; }
        public static int Height { get; set; }

        const int MAX_SIZE = 1000;
        const int DEFAULT_WIDTH = 800;
        const int DEFAULT_HEIGHT = 600;

        public static void Init(Form form)
        {
            WinForm.form = form;
            // Графическое устройство для вывода графики
            Graphics g;

            // Предоставляет доступ к главному буферу графического контекста для текущего приложения
            _context = BufferedGraphicsManager.Current;
            g = form.CreateGraphics();

            // Создаем объект (поверхность рисования) и связываем его с формои
            // Запоминаем размеры формы + добавил выбрасывание исключения при больших размерах
            try {
                Width = form.Width;
                Height = form.Height;

                if (Width > MAX_SIZE || Height > MAX_SIZE || Width < 0 || Height < 0){
                    throw new ArgumentOutOfRangeException();
                } 
            } catch ( ArgumentOutOfRangeException ){
                Console.WriteLine($"Заданы не корректные размеры формы. Будут использованы размеры по умолчанию ({DEFAULT_WIDTH}x{DEFAULT_HEIGHT})");
                Width = form.Width = DEFAULT_WIDTH;
                Height = form.Height = DEFAULT_HEIGHT;
            }

            // Связываем буфер в памяти с графическим объектом, чтобы рисовать в буфере
            Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));
        }
    }


}
