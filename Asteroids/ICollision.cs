﻿using System;
using System.Drawing;

namespace Asteroids
{
    /// <summary>
    ///     Интерфейс определения стлкновения объектов
    /// </summary>
    public interface ICollision
    {
        bool Collision(ICollision obj);
        Rectangle Rect { get; }
    }
}
