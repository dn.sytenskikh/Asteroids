﻿using System;
using System.IO;

namespace Asteroids
{
    public delegate void WriteJournal<T>(T message);

    public static class Journal
    {
        public static void WriteToConsole(String message)
        {
            Console.WriteLine(message);
        }

        public static void WriteToFile(String message)
        {
            try 
            {
                using (StreamWriter sw = new StreamWriter("/tmp/journal", true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(message);
                }
            } 
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
