﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;

namespace Asteroids
{
    /// <summary>
    /// Игровой цикл, инициализация объектов формы
    /// </summary>
    static class Game
    {
        private static BaseObject[] objs;
        private static List<Bullet> bullets = new List<Bullet>();
        private static List<Asteroid> asteroids;
        private static Spaceship spaceship;
        private static MedicineChest medicineChest;

        private static int asteroidsCount = 10;

        public static int Score { get; private set; } = 0;

        private static WriteJournal<String> journal = Journal.WriteToConsole;

        private static Timer timer = new Timer { Interval = 100 };
        public static Random r = new Random();

        static void Timer_Tick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        public static void Init()
        {
            Load();

            timer.Start();
            timer.Tick += Timer_Tick;

            WinForm.form.KeyDown += Form_KeyDown;
            Spaceship.MessageDie += Finish;

            journal("Game Init");
        }

        static void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey){
                bullets.Add(new Bullet(new Point(spaceship.Rect.X + 10, spaceship.Rect.Y + 4), new Point(30, 0), new Size(20, 5)));
            }
            if (e.KeyCode == Keys.Up) spaceship.Up();
            if (e.KeyCode == Keys.Down) spaceship.Down();
        }

        public static void Finish()
        {
            timer.Stop();
            WinForm.Buffer.Graphics.DrawString("The End", new Font(FontFamily.GenericSansSerif, 60, FontStyle.Underline), Brushes.White, 200, 100);
            WinForm.Buffer.Render();

            journal("Game Finish");
        }

        /// <summary>
        /// Создание всех объектов на форме
        /// </summary>
        private static void Load()
        {
            objs = new BaseObject[30];
            for (int i = 0; i < objs.Length - 1; i++)
                objs[i] = new Star(new Point(r.Next(WinForm.Width), r.Next(WinForm.Height)), new Point(-i, 0), new Size(5, -5));

            objs[29] = new Earth(new Point(400, 200), new Point(-r.Next(4), 0), new Size(50, 50));

            CreateAsteroids();
            spaceship = new Spaceship(new Point(10, 400), new Point(5, 5), new Size(20, 20));
            medicineChest = new MedicineChest(new Point(WinForm.Width, r.Next(0, WinForm.Height)), new Point(-r.Next(4, 8), r.Next(5, 10)), new Size(40, 40), -r.Next(10, 20));

            journal("Game Load");
        }

        private static void Draw()
        {
            WinForm.Buffer.Graphics.Clear(Color.Black);

            foreach (BaseObject obj in objs)
                obj.Draw();

            foreach (Asteroid a in asteroids)
                a?.Draw();

            foreach (Bullet bullet in bullets){
                bullet.Draw();
            }
            spaceship.Draw();
            medicineChest?.Draw();

            if (spaceship != null)
                WinForm.Buffer.Graphics.DrawString($"Energy {spaceship.Energy}", SystemFonts.DefaultFont, Brushes.White, 0, 0);

            WinForm.Buffer.Render();
        }

        private static void Update()
        {
            foreach (BaseObject obj in objs)
                obj.Update();

            foreach (Bullet bullet in bullets){
                bullet.Update();
            }

            medicineChest?.Update();

            if (medicineChest != null && spaceship.Collision(medicineChest))
            {
                spaceship.EnergyLow(medicineChest.Power);
                System.Media.SystemSounds.Asterisk.Play();

                medicineChest = null;

                journal($"SpaceShip Medicine Chest. Energy: {spaceship.Energy}");
            }

            for (int i = 0; i < asteroids.Count; i++)
            {
                bool asteroidHit = false;
                asteroids[i].Update();

                for (int j = 0; j < bullets.Count; j++){

                    if (bullets[j].Collision(asteroids[i]))
                    {   
                        System.Media.SystemSounds.Hand.Play();

                        asteroids.RemoveAt(i--);
                        bullets.RemoveAt(j--);

                        asteroidHit = true;
                        AsteroidHit();

                        break;
                    }
                }

                if (asteroidHit) continue;

                if(spaceship.Collision(asteroids[i])){
                    var rnd = new Random();
                    spaceship.EnergyLow(asteroids[i].Power);

                    asteroids.RemoveAt(i--);

                    System.Media.SystemSounds.Asterisk.Play();

                    journal($"SpaceShip Hit. Energy: {spaceship.Energy}");

                    if (spaceship.Energy <= 0) {
                        spaceship.Die();
                        journal($"SpaceShip DIE");
                    }
                }
            }

            if (asteroids.Count == 0)
            {
                bullets = new List<Bullet>();
                CreateAsteroids();
            }
        }

        private static void AsteroidHit(){
            journal("Asteroid Hit");
            Score += 1;
        }

        private static void CreateAsteroids(){
            asteroids = new List<Asteroid>(asteroidsCount);
            for (var i = 0; i < asteroidsCount; i++)
            {
                asteroids.Add(new Asteroid(new Point(WinForm.Width, r.Next(0, WinForm.Height)), new Point(-r.Next(4, 8), r.Next(5, 10)), new Size(40, 40), r.Next(10, 20)));
            }
            asteroidsCount++;
        }
    }
}

