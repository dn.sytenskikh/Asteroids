﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Asteroids
{
    /// <summary>
    ///     Экран заставки
    /// </summary>
    static class SplashScreen
    {

        static public void Init(Form form){
            WinForm.Buffer.Graphics.Clear(Color.Black);

            CreateButtons(form);
            AddLabelName(form);

            WinForm.Buffer.Render();

        }

        public static void CreateButtons(Form form)
        {
            SplashScreen.CreateStartButton(form);
            SplashScreen.CreateExitButton(form);
        }

        private static void CreateStartButton(Form form)
        {
            Button button = new Button
            {
                Text = "Start",
                Visible = true,
                Size = new Size(50, 50),
                Location = new Point(375, 275)
            };

            form.Controls.Add(button);
            button.Click += delegate 
            {
                form.Controls.Clear();
                Game.Init();
                
            };
        }

        private static void CreateExitButton(Form form)
        {
            Button button = new Button
            {
                Text = "Exit",
                Visible = true,
                Size = new Size(50, 50),
                Location = new Point(375, 375),
            };

            form.Controls.Add(button);
            button.Click += delegate
            {
                Application.Exit();
            };
        }

        public static void AddLabelName(Form form)
        {
            Label label = new Label
            {
                Text = "Created by Dmitry Sytenskih",
                Visible = true,
                Size = new Size(200, 100),
                Location = new Point(600, 520)
            };
            form.Controls.Add(label);
        }

    }
}
