﻿using System;
using System.Windows.Forms;

namespace Asteroids
{
    /// <summary>
    ///     Точка входа в программу
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            Form form = new Form
            {
                Width = 800,
                Height = 600
            };
            WinForm.Init(form);
            form.Show();
            SplashScreen.Init(form);
            Application.Run(form);
        }
    }
}
