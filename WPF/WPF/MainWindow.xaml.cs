﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Employee> emp;
        ObservableCollection<string> dep;

        public MainWindow()
        {
            InitializeComponent();

            emp = new ObservableCollection<Employee>();
            dep = new ObservableCollection<string>();

            listEmployee.ItemsSource = emp;
            listDepartment.ItemsSource = dep;

            emp.Add(new Employee("Vasiliy"));

            dep.Add("Department1");
        }

        private void addEmployee_Click(object sender, RoutedEventArgs e)
        {
            if (textEmployee.Text.Length > 0)
            {
                emp.Add(new Employee(textEmployee.Text));
                textEmployee.Text = "";
            }
        }

        private void addDepartment_Click(object sender, RoutedEventArgs e)
        {
            if (textDepartment.Text.Length > 0)
            {
                dep.Add(textDepartment.Text);
                textDepartment.Text = "";
            }
        }

        private class Employee
        {
            public string Name { get; set; }

            public string Department { get; set; }

            public Employee(string name)
            {
                Name = name;
            }

            public override string ToString()
            {
                return $"Сотрудник {Name} принадлежит департаменту {Department}";
            }
        }
    }
}
